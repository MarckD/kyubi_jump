﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class dino : MonoBehaviour
{
    private Rigidbody2D rb2d = null;
    private float move = 0f;
    public float maxS = 11f;
    private bool jump;
    //private bool attack;
    public float fuerzaSalto = 5.0f;
    [SerializeField] private GameObject graphics;
    [SerializeField] private Animator animator;
    public bool isGrounded = false;
    public bool isDead = false;
    private float scalaActual;

    //[SerializeField] AudioSource audio;

    void Awake()
    {
        rb2d = GetComponent<Rigidbody2D>();
    }
    
    void FixedUpdate()
    {
        if (isDead) 
        { 
            return; 
        }

        //rb2d.velocity = new Vector2(move * maxS, rb2d.velocity.y);
        rb2d.AddForce(Vector2.right * move * maxS);

        animator.SetInteger("Velocidad", (int)Math.Abs(rb2d.velocity.x));
    }

    private void Update()
    {
        if (isDead) 
        { 
            return; 
        }
        
        move = Input.GetAxis("Horizontal");
        jump = Input.GetKeyDown(KeyCode.Space);
        //attack = Input.GetButton("Fire1");

        //if (attack && isGrounded)
        // {
        //     move = 0f;
        //    jump = false;
        //    animator.SetBool("attack", attack);
        //}
        //else
        //{
        //    animator.SetBool("attack", false);
        //}
    

        if (jump && isGrounded)
        {
            Debug.LogError("Salta");
            jump = false;
            rb2d.AddForce(Vector2.up * fuerzaSalto, ForceMode2D.Impulse);
        }

        scalaActual = graphics.transform.localScale.x;

        if (move > 0 && scalaActual < 0)
        {
            graphics.transform.localScale = new Vector3(1, 1, 1);
        }
        else if (move < 0 && scalaActual > 0)
        {
            graphics.transform.localScale = new Vector3(-1, 1, 1);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        isGrounded = true;
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        isGrounded = false;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "enemy")
        {
            //enemigo inmobil
            animator.SetTrigger("dead");
            isDead = true;
        }
        else if (collision.gameObject.tag == "PowerUp")
        {
            //power up inmobil
            maxS *= 4;
            Destroy(collision.gameObject);
        }
        else if(collision.gameObject.tag == "saltante")
        {
            rb2d.AddForce(Vector2.up * move * maxS * 4);
        }
    }
    //public void SonidoMuerte()
    //{
    //    audio.Play();
    //}
}
